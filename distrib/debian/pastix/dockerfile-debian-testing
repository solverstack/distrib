FROM debian:testing

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update -y
RUN apt-get install git build-essential wget tar curl devscripts dh-make quilt pbuilder sbuild lintian svn-buildpackage git-buildpackage -y
RUN apt-get install gfortran cmake python-is-python3 pkg-config libopenblas-dev liblapacke-dev libstarpu-dev libopenmpi-dev libhwloc-dev libscotch-dev -y

ARG GITLABTOKEN
ARG GITLABPROJNUM=987
ARG DEBIANDIST=debian_testing
ARG RELEASEVER="6.4.0"
ARG DEBIANARCH="1_amd64"
ARG RELEASETAR=pastix-$RELEASEVER.tar.gz
ARG RELEASETARDEB=pastix_$RELEASEVER.orig.tar.gz
ARG RELEASEURL=https://files.inria.fr/pastix/releases/v6/pastix-6.4.0.tar.gz

RUN wget $RELEASEURL
RUN mv $RELEASETAR $RELEASETARDEB
RUN tar xvf $RELEASETARDEB

ENV LOGNAME=root
ENV DEBEMAIL=florent.pruvost@inria.fr
RUN cd /pastix-$RELEASEVER/ && dh_make --library --yes

COPY changelog /pastix-$RELEASEVER/debian/
COPY control /pastix-$RELEASEVER/debian/
COPY copyright /pastix-$RELEASEVER/debian/
COPY rules /pastix-$RELEASEVER/debian/

RUN export DEB_BUILD_OPTIONS='nocheck' && cd /pastix-$RELEASEVER/ && debuild -us -uc

RUN apt-get install ./pastix_${RELEASEVER}-${DEBIANARCH}.deb -y

RUN git clone https://gitlab.inria.fr/solverstack/distrib.git
RUN cd /distrib/cmake/test/pastix && mkdir build && cd build && cmake .. && make && ./test_pastix --lap 100

RUN curl --header "PRIVATE-TOKEN: ${GITLABTOKEN}" --upload-file ./pastix_${RELEASEVER}-${DEBIANARCH}.deb "https://gitlab.inria.fr/api/v4/projects/${GITLABPROJNUM}/packages/generic/${DEBIANDIST}/${RELEASEVER}/pastix_${RELEASEVER}-${DEBIANARCH}.deb"
