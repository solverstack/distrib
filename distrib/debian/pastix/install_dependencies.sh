#!/bin/sh
# whatis: standard development environment to use the pastix library

sudo apt-get update -y
sudo apt-get install build-essential git gfortran cmake doxygen python-is-python3 pkg-config libopenblas-dev liblapacke-dev libstarpu-dev libopenmpi-dev libhwloc-dev libscotch-dev -y
# libopenblas-dev can be replaced by libmkl-dev or liblapack-dev

# install pastix with cmake
git clone --recursive https://gitlab.inria.fr/solverstack/pastix.git
cd pastix && cmake -S . -B build -DPASTIX_INT64=OFF -DPASTIX_WITH_MPI=ON -DPASTIX_WITH_STARPU=ON -DBUILD_SHARED_LIBS=ON && cmake --build build -j5 && sudo cmake --install build

# example usage: use pastix library in your own cmake project (we provide a PASTIXConfig.cmake)
cd ..
git clone https://gitlab.inria.fr/solverstack/distrib.git
cd distrib/cmake/test/pastix && mkdir build && cd build && cmake .. && make && ./test_pastix --lap 100

# example usage: use pastix library in your own not cmake project
# use pkg-config to get compiler flags and linking
pkg-config --cflags pastix
pkg-config --libs pastix
# if there are static libraries use the --static option of pkg-config
