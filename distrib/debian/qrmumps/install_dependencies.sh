#!/bin/sh
# whatis: standard development environment to use the qrmumps library

sudo apt-get update -y
sudo apt-get install build-essential git gfortran cmake emacs perl pkg-config wget libopenblas-dev libstarpu-dev libscotch-dev libmetis-dev libsuitesparse-dev -y
# libopenblas-dev can be replaced by libmkl-dev or liblapack-dev

# install qrmumps with cmake
wget https://gitlab.com/qr_mumps/qr_mumps/-/archive/3.1/qr_mumps-3.1.tar.gz
tar xvf qr_mumps-3.1.tar.gz
cd qr_mumps-3.1
cmake -S . -B build -DQRM_WITH_STARPU=ON -DBUILD_SHARED_LIBS=ON && cmake --build build -j5 && sudo cmake --install build

# example usage: use qr_mumps library in your own cmake project (we provide a qrmConfig.cmake)
cd ..
git clone https://gitlab.inria.fr/solverstack/distrib.git
cd distrib/cmake/test/qrmumps && mkdir build && cd build && cmake .. && make && ./sqrm_least_squares_basic
