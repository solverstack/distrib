FROM ubuntu:22.04

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update -y
RUN apt-get install git build-essential wget tar curl devscripts dh-make quilt pbuilder sbuild lintian svn-buildpackage git-buildpackage -y
RUN apt-get install perl gfortran cmake pkg-config libopenblas-dev libstarpu-dev libscotch-dev libmetis-dev libsuitesparse-dev -y

ARG GITLABTOKEN
ARG GITLABPROJNUM=30307
ARG DEBIANDIST=ubuntu_22.04
ARG RELEASEVER="3.1"
ARG DEBIANARCH="1_amd64"
ARG RELEASETAR=qr_mumps-$RELEASEVER.tar.gz
ARG RELEASETARDEB=qrmumps_$RELEASEVER.orig.tar.gz
ARG RELEASEURL=https://gitlab.com/qr_mumps/qr_mumps/-/archive/$RELEASEVER/$RELEASETAR

RUN wget $RELEASEURL
RUN tar xvf $RELEASETAR
RUN mv /qr_mumps-$RELEASEVER/ /qrmumps-$RELEASEVER/
RUN tar czf $RELEASETARDEB qrmumps-$RELEASEVER/

ENV LOGNAME=root
ENV DEBEMAIL=florent.pruvost@inria.fr
RUN cd /qrmumps-$RELEASEVER/ && dh_make --library --yes

COPY changelog /qrmumps-$RELEASEVER/debian/
COPY control /qrmumps-$RELEASEVER/debian/
COPY copyright /qrmumps-$RELEASEVER/debian/
COPY rules /qrmumps-$RELEASEVER/debian/

RUN export DEB_BUILD_OPTIONS='nocheck' && cd /qrmumps-$RELEASEVER/ && debuild -us -uc

RUN apt-get install ./qrmumps_$RELEASEVER-$DEBIANARCH.deb -y
RUN dqrm_least_squares_basic

RUN curl --header "PRIVATE-TOKEN: ${GITLABTOKEN}" --upload-file ./qrmumps_$RELEASEVER-$DEBIANARCH.deb "https://gitlab.inria.fr/api/v4/projects/$GITLABPROJNUM/packages/generic/$DEBIANDIST/$RELEASEVER/qrmumps_$RELEASEVER-$DEBIANARCH.deb"