# How to test spack packages

Build the Docker image from [dockerfile-ubuntu24.04](dockerfile-ubuntu24.04)
```sh
docker build -t registry.gitlab.inria.fr/solverstack/distrib/spack -f distrib/spack/dockerfile-ubuntu24.04 .
docker login registry.gitlab.inria.fr
docker push registry.gitlab.inria.fr/solverstack/distrib/spack
```

Then gitlab-ci jobs "package_spack" can be triggered manually in the [pipelines](https://gitlab.inria.fr/solverstack/distrib/-/pipelines).

Or locally in a docker container
```sh
docker pull registry.gitlab.inria.fr/solverstack/distrib/spack
docker run -it registry.gitlab.inria.fr/solverstack/distrib/spack
spack install scotch
spack install starpu
spack install chameleon
spack install pastix
spack install qrmumps
spack install fabulous
spack install paddle
spack install composyx
```
