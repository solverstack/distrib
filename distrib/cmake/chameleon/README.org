#+TITLE: CHAMELEON CMake installer

Requirements:
- Linux with curl, tar, wget, git, bison, flex, patch, perl,
  sed, python, pkg-config, cmake commands
- C, C++ and Fortran compilers (e.g. gcc, g++, gfortran)
- blas, lapack, cblas, lapacke (e.g. openblas, mkl), hwloc libraries

Optional:
- mpi (e.g. openmpi, mpich)
- cuda and cublas

See our [[./Dockerfile]] for an example of Linux environment to build
compose.

Usage:
#+begin_src sh
git clone https://gitlab.inria.fr/solverstack/distrib.git
cd distrib/distrib/cmake/chameleon
mkdir build
cd build
cmake ..
make
#+end_src
Notice all programs will be installed in the default directory
(/usr/local/).

To change the default directory use ~CMAKE_INSTALL_PREFIX~:
#+begin_src sh
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$PWD/install
make
#+end_src

If you don't have an internet connection you can use the sources embedded:
#+begin_src sh
cmake .. -DNOWEB=ON
#+end_src

To build in a "debug" mode:
#+begin_src sh
cmake .. -DCMAKE_BUILD_TYPE=Debug
#+end_src

Example with CUDA enabled and MPI disabled:
#+begin_src sh
cmake .. -DCUDA=ON -DMPI=OFF
#+end_src

Example with Intel MKL (install libmkl-dev):
#+begin_src sh
cmake .. -DBLA_VENDOR=Intel10_64lp
#+end_src

Example with OpenBLAS:
#+begin_src sh
cmake .. -DBLA_VENDOR=OpenBLAS
#+end_src

Remark: when trying several configurations subsequently, like changing
the Blas/Lapack implementation, a cleaning of the build directory may
be necessary:
#+begin_src sh
cd build
rm * -rf
cmake .. -DBLA_VENDOR=Intel10_64lp
make
#+end_src

To generate a tarball of the suite with all dependencies sources
#+begin_src sh
./download.sources.sh
#+end_src
Then create a tarball of the current directory, for example if the
directory is chameleonsuite
#+begin_src sh
cd ..
tar czf composesuite.tar.gz composesuite/
#+end_src
