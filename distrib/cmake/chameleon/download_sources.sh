#!/bin/bash

set -x

wget https://files.inria.fr/starpu/starpu-1.4.7/starpu-1.4.7.tar.gz
tar xvf starpu-1.4.7.tar.gz
rm starpu-1.4.7.tar.gz
wget https://gitlab.inria.fr/api/v4/projects/616/packages/generic/source/v1.3.0/chameleon-1.3.0.tar.gz
tar xvf chameleon-1.3.0.tar.gz
rm chameleon-1.3.0.tar.gz
