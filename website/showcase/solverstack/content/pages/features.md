Title: Features
attribute: 2

# [Chameleon](https://gitlab.inria.fr/solverstack/chameleon)
<b>Dense linear algebra</b>

  <table>
    <tr>
      <td>
        <ul>
        <li>Written in C, Fortran interface, CMake</li>
        <li>Algorithms: GEMM, POTRF, GETRF, GEQRF, GESVD, ...</li>
        <li>Matrices forms: general, symmetric, triangular</li>
        <li>Precisions: s, d, c, z</li>
        <li>Runtime systems: PaRSEC, StarPU</li>
        <li>Distributed MPI and CUDA capabilities</li>
        </ul>
      </td>
      <td>
        <img src="./matrix-step-12.png" width=270 height=480>
        <p align=center>Task-based Cholesky (POTRF) algorithm</p>
      </td>
    </tr>
  </table>

# [PaStiX](https://gitlab.inria.fr/solverstack/pastix)
<b>Sparse linear algebra, supernodal direct solver</b>

  <table>
    <tr>
      <ul>
      <li>Written in C, Fortran/Python/Julia interfaces, CMake</li>
      <li>Algorithms: POTRF, GETRF, TRSM, ...</li>
      <li>Matrices forms: general, symmetric, triangular</li>
      <li>Storage formats: CSC, CSR, and IJV</li>
      <li>Precisions: s, d, c, z</li>
      <li>Low-Rank compression</li>
      <li>Runtime systems: PaRSEC, StarPU</li>
      <li>Distributed MPI and CUDA capabilities</li>
      <li>Ordering/partitioning: Metis, Scotch</li>
      </ul>
    </tr>
  </table>

  <table>
    <tr>
      <td>
        <img src="./SymbolicFactorization.png" width=830 height=510>
        <p align=center>Symbolic factorization</p>
      </td>
    </tr>
  </table>

# [qr_mumps](http://buttari.perso.enseeiht.fr/qr_mumps/)
<b>Sparse linear algebra, multifrontal direct solver</b>

  <table>
    <tr>
      <td>
        <ul>
        <li>Written in Frotran 2008, C interface, CMake</li>
        <li>Algorithms: GEMM, POTRF, GETRF, GEQRF, ...</li>
        <li>Matrices forms: general, symmetric, triangular</li>
        <li>Precisions: s, d, c, z</li>
        <li>Runtime system: StarPU</li>
        <li>Multithreaded, CUDA capabilities</li>
        <li>Ordering/Partitioning: Colamd, Metis, Scotch</li>
        </ul>
      </td>
      <td>
        <img src="./qr_mumps_dag.png" width=394 height=512>
        <p align=center>qr_mumps Direct Acyclic Graph</p>
      </td>
    </tr>
  </table>

# [MaPHyS++](https://gitlab.inria.fr/solverstack/maphys/composyx)
<b>Sparse linear algebra, algebraic domain decomposition</b>

  - Written in C++, C and Fortran interfaces, CMake
  - Use Direct solvers: MUMPS, PaStiX or qr_mumps
  - Use Krylov solver: fabulous
  - Use Eigen solver: arpack (preconditioning)
  - Matrices forms: general, symmetric
  - Storage formats: IJV, armadillo, eigen3
  - Precisions: s, d, c, z
  - Distributed MPI, hybrid MPI/threads

  <table>
    <tr>
      <td>
        <img src="./maphys_ddm.png" width=906 height=377>
        <p align=center>Algebraic domain decomposition</p>
      </td>
    </tr>
  </table>

# [fabulous](https://gitlab.inria.fr/solverstack/fabulous)
<b>Sparse linear algebra, Block Krylov iterative solver</b>

  - Written in C++, C and Fortran interfaces, CMake
  - This Library currently implements various variants of Block
    Krylov iterative solvers:
    - BCG (Block Conjugate Gradient)
    - BF-BCG (Breadown Free BCG)
    - BGCR (Block Generalized Conjugate Residual)
    - BGMRES (Block General Minimum Residual)
    - IB-BGMRES (BGMRES with inexact breakdown)
    - BGMRES-DR (BGMRES with deflated restarting)
    - IB-BGMRES-DR (BGMRES with inexact breakdown and deflated
      restarting)
    - IB-BGCRO-DR (Block Generalized Conjugate Residual Method with
      Inner Orthogonalization with inexact breakdown and deflated
      restarting)

# [StarPU](https://starpu.gitlabpages.inria.fr/)
<b>Runtime system for heterogeneous multicore architectures</b>

  <table>
    <tr>
      <td>
        <ul>
          <li>Written in C, Fortran interface, GNU autotools</li>
          <li>Supported on GNU/Linux, Mac OS X and Windows</li>
          <li>Scheduling: eager, work stealing, prio, dmda, ...</li>
          <li>Data structures vectors, dense matrices, CSR/BCSR/COO sparse matrices, specific, ...</li>
          <li>Enforces memory coherency over the machine</li>
          <li>Distributed MPI, CUDA, OpenCL, OpenMP interface</li>
          <li>Out of core capabilities, supports HDF5 for I/Os</li>
        </ul>
      </td>
      <td>
        <img src="./starpu1.png" width=300 height=300>
        <p align=center>A StarPU overview</p>
      </td>
    </tr>
  </table>

# [Scotch](https://gitlab.inria.fr/scotch/scotch)
<b>Graph partitioning and matrix ordering</b>

  - Written in C, Fortran interface, Makefile
  - Provides algorithms to partition graph structures, as well as
    mesh structures defined as node-element bipartite graphs and
    which can also represent hypergraphs
  - Can map any weighted source graph onto any weighted target graph
  - Computes amalgamated block orderings of sparse matrices, for
    efficient solving using BLAS routines
  - Offers extended support for adaptive graphs and meshes through
    the handling of disjoint edge arrays
  - Running time is linear in the number of edges of the source
    graph, and logarithmic in the number of vertices of the target
    graph for mapping computations
  - Distributed MPI (PT-Scotch)

  <table>
    <tr>
      <td>
        <img src="./scotch2.gif" width=780 height=630>
        <p align=center>Graph partitioning of a car</p>
      </td>
    </tr>
  </table>
