Title: Solverstack@Inria Bordeaux
URL:
save_as: index.html
attribute: 1

**Solverstack@Inria Bordeaux** aims at providing a C/C++ and Fortran
software distribution of parallel algorithms for computing dense and
sparse linear algebra and for solving linear systems with
factorization techniques LL^T, LU, QR, Least Squares, SVD, EVD. One of
the main objective is the *portability of performances*, from labtops
to supercomputers. The softwares are mainly developed by researchers
and engineers working in, or in strong collababoration with,
[Inria](https://www.inria.fr/fr) project teams
[HiePACS](https://team.inria.fr/hiepacs/),
[Storm](https://team.inria.fr/storm/) and
[TADaaM](https://team.inria.fr/tadaam/) located at [Inria Bordeaux
Sud-Ouest](https://www.inria.fr/centre/bordeaux).

![Solverstack DAG](./solverstack.svg)

**Solverstack** is actually a collection of software libraries with
some dependencies between them. This software collection gathers:

 - linear algebra on dense matrices with [Chameleon](https://gitlab.inria.fr/solverstack/chameleon),
 - linear algebra on sparse matrices with [PaStiX](https://gitlab.inria.fr/solverstack/pastix) and [qr_mumps](http://buttari.perso.enseeiht.fr/qr_mumps/),
 - Krylov solvers [fabulous](https://gitlab.inria.fr/solverstack/fabulous),
 - hybrid direct/iterative solver based on algebraic domain decomposition techniques with [MaPHyS++](https://gitlab.inria.fr/solverstack/maphys/composyx),
 - tasks scheduling with the [StarPU](https://starpu.gitlabpages.inria.fr/) runtime system,
 - graphs partitioning and sparse matrix ordering with [Scotch](https://gitlab.inria.fr/scotch/scotch).

<!-- ![Solverstack DAG](./solverstack.svg) -->
