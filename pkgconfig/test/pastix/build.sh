#!/bin/sh
# use --static as parameter if pasix is static (.a)
# export PKG_CONFIG_PATH=...

CFLAGS=`pkg-config $1 --cflags pastix`
LIBS=`pkg-config $1 --libs pastix`
gcc $CFLAGS simple.c $LIBS -o simple
gcc $CFLAGS simple_solve_and_refine.c $LIBS -o simple_solve_and_refine

#export LD_LIBRARY_PATH=.../pastix/install/lib:...spm/install/lib
#./simple --lap 100
#mpiexec -n 2 --bind-to none ./simple --lap 100 -t 2

FFLAGS=`pkg-config $1 --cflags pastixf`
LIBS=`pkg-config $1 --libs pastixf`
gfortran $FFLAGS fsimple.F90 $LIBS -o fsimple

#./fsimple --lap 100
