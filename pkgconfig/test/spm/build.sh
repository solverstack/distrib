#!/bin/sh
# use --static as parameter if spm is static (.a)
# export PKG_CONFIG_PATH=...
SPM_CFLAGS=`pkg-config $1 --cflags spm`
SPM_LIBS=`pkg-config $1 --libs spm`
gcc $SPM_CFLAGS test_spm.c $SPM_LIBS -o test_spm
#export LD_LIBRARY_PATH=.../spm/install/lib 
#./test_spm
SPM_FFLAGS=`pkg-config $1 --cflags spmf`
SPM_LIBS=`pkg-config $1 --libs spmf`
gfortran $SPM_FFLAGS test_spmf.f90 $SPM_LIBS -o test_spmf
