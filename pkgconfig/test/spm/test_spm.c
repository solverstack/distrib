/**
 *
 * @file test_spm.c
 *
 * Testing one call to SParse Matrix package.
 *
 * @copyright (c) 2020 Inria. All rights reserved.
 *
 * @version 1.0.0
 * @author Mathieu Faverge
 * @author Florent Pruvost
 * @date 2020-12-15
 *
 */
#include <stdlib.h>
#include <stdio.h>
#include <spm.h>

int
main(int argc, char ** argv)
{
    char           *filename;
    spm_driver_t   driver;
    spmatrix_t     *spm;
    void           *x = NULL;
    double          normA;
    size_t          size;
    int             nrhs  = 1;
    int             rc    = 0;

#ifdef SPM_WITH_MPI
    MPI_Init(NULL, NULL);
#endif

    if ( argc >= 3 ) {
        driver = atoi( argv[1] );
        filename = argv[2];
    }
    else {
        driver = SpmDriverLaplacian;
        filename = "d:10:10:10:4.";
    }

    /**
     * Read the sparse matrix through a driver
     */
    spm = malloc( sizeof( spmatrix_t ) );
    spmReadDriver( driver, filename, spm );

    /**
     * Print a summary of information about the spm
     */
    spmPrintInfo( spm, stdout );

    /**
     * Generate a fake values array if needed for the numerical part
     */
    if ( spm->flttype == SpmPattern ) {
        spmGenFakeValues( spm );
    }

    /**
     * Compute the frobenius norm of the spm
     */
    normA = spmNorm( SpmFrobeniusNorm, spm );

    /**
     * Scale the matrix by the norm.
     */
    spmScalMatrix( 1. / normA, spm );

    spmExit( spm );
    free( spm );

#ifdef SPM_WITH_MPI
    MPI_Finalize();
#endif

    if ( rc == 0 ) {
        return EXIT_SUCCESS;
    }
    else {
        printf( "hqr test failed !!!, ret %d\n", rc );
        return EXIT_FAILURE;
    }
}
