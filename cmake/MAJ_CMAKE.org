#+TITLE: Mise à jour CMake
#+OPTIONS: H:3 num:t toc:t \n:nil @:t ::t |:t _:nil ^:nil -:t f:t *:t <:t
#+SETUPFILE: theme-bigblow.setup

Ce qui peut être bon à savoir pour effectuer la mise à jour.

1) Doc morse cmake
   - [[https://gitlab.inria.fr/solverstack/morse_cmake/-/blob/master/modules/find/morse_cmakefind_doc.org]]
2) Certains modules ont été supprimés:
   - BLAS, LAPACK car ceux embarqués par défaut dans cmake (assez
     récent) sont suffisamment performants
   - ceux concernant des logiciels HiePACS compilés/installés avec
     cmake car ceux-ci doivent maintenant fournir obligatoirement un
     fichier FOOConfig.cmake à l'installation (équivalent cmake du
     pkgconfig) voir plus bas point 7)
3) Les Find de morse fournissent systématiquement des "IMPORTED
   TARGET", nommé MORSE::FOO pour FindFOO.cmake, utilisables par les
   projets appelant. Ainsi pour linker avec StarPU, plus besoin de
   jouer avec les différentes listes cmake contenant des infos, on
   utilise directement comme ceci dans son projet

   #+begin_example
   find_package(MPI REQUIRED) # module officiel de cmake
   find_package(LAPACKE REQUIRED) # module morse
   find_package(STARPU "1.3" REQUIRED) # 1.3 est le niveau d'API starpu nécessaire et suffisant pour l'utilisateur

   add_executable(test_starpu test_starpu.c)
   target_link_libraries(test_starpu PRIVATE MORSE::STARPU MORSE::LAPACKE MPI::MPI_C)
   #+end_example

   Cela va gérer l'ensemble des propriétés (les -I, -L, autres
   options éventuelles) automatiquement. Le mot clef PRIVATE indique
   que la cible 'test_starpu', qui est un executable, dépend de
   starpu à la compilation seulement.

   Note: si la cible était une bibliothèque qui expose starpu dans
   son API, via des include starpu.h dans ses entêtes publiques par
   exemple, alors il faudrait remplacer PRIVATE par PUBLIC. Pour
   créer une lib header only avec add_library, le mot clef INTERFACE
   pourra être utilisé.
4) Pour les dépendances non installées via cmake et fournissant leur
   configuration via pkg-config le Find force à utiliser le
   méchanisme de pkg-config (eztrace, fxt, gtg, hwloc, papi, parsec,
   simgrid, starpu), c'est plus propre.
5) Lorsque'il existe à la fois les libs en statiques et en dynamique
   d'installées, on peut utiliser la variable cmake FOO_STATIC afin
   de forcer l'utilisation des libs statiques pour FOO
6) Pour les blas/lapack, dans la situation où vous faites cohabiter
   plusieurs implémentations disponibles dans votre environnement,
   n'hésitez pas à vous référer à la doc des modules officiels pour
   choisir lequel privilégier avec BLA_VENDOR (OpenBLAS,
   Intel10_64ilp, Intel10_64ilp_seq, FLAME, Generic, ...)

   #+begin_example
   cmake --help-module FindBLAS
   cmake --help-module FindLAPACK
   #+end_example
7) Nos packages doivent exporter leurs target et installer le
   fichier de config donnant aux utilisateurs accès à ces
   targets. Prenons l'exemple de SPM qui est assez simple. Pour la
   lib on exporte la cible comme ceci
   cf. [[https://gitlab.inria.fr/solverstack/spm/-/blob/master/src/CMakeLists.txt]]

   #+begin_example
   # export target spm
   install(EXPORT spmTargets
           FILE spmTargets.cmake
           NAMESPACE SPM::
           DESTINATION lib/cmake/spm
           )

   # install target file
   install(TARGETS spm EXPORT spmTargets)
   #+end_example

   On fait la même chose pour les différentes libs du projet.

   Puis au niveau du projet principal (racine) on créé son fichier
   de config, par exemple
   cf. [[https://gitlab.inria.fr/solverstack/spm/-/blob/master/CMakeLists.txt]]

   #+begin_example
   ### Export targets
   # see https://cmake.org/cmake/help/latest/module/CMakePackageConfigHelpers.html
   include(CMakePackageConfigHelpers)

   set(BIN_INSTALL_DIR "bin/" CACHE STRING "where to install executables relative to prefix" )
   set(INC_INSTALL_DIR "include/" CACHE STRING "where to install headers relative to prefix" )
   set(LIB_INSTALL_DIR "lib/" CACHE STRING "where to install libraries relative to prefix" )

   configure_package_config_file(cmake_modules/SPMConfig.cmake.in
                                 ${CMAKE_CURRENT_BINARY_DIR}/SPMConfig.cmake
                                 INSTALL_DESTINATION ${LIB_INSTALL_DIR}/cmake/spm
                                 PATH_VARS BIN_INSTALL_DIR INC_INSTALL_DIR LIB_INSTALL_DIR)
   write_basic_package_version_file(SPMConfigVersion.cmake
                                    VERSION ${SPM_VERSION}
                                    COMPATIBILITY AnyNewerVersion)

   # Install config files
   install(FILES ${CMAKE_CURRENT_BINARY_DIR}/SPMConfig.cmake ${CMAKE_CURRENT_BINARY_DIR}/SPMConfigVersion.cmake
           DESTINATION ${LIB_INSTALL_DIR}/cmake/spm)

   # need MORSE Find modules: necessary files must be distributed in the install path
   set(spm_dependencies "M;CBLAS;LAPACKE")
   morse_install_finds(spm_dependencies ${LIB_INSTALL_DIR}/cmake/spm/find)
   #+end_example
   Remarquez la commande morse_install_finds à la fin qui permet
   d'installer les Find morse nécessaires si vous en
   dépendez. L'utilisateur pourra ainsi en bénéficier pour trouver
   votre lib sans pour autant avoir à embarquer ces Find dans son
   propre projet.

   SPMConfig.cmake.in défini comme suit
   [[https://gitlab.inria.fr/solverstack/spm/-/blob/master/cmake_modules/SPMConfig.cmake.in]]

   Avec ce fichier de config installé un développeur utilisant CMake
   peut à présent très facilement trouver la lib, via le
   CMAKE_PREFIX_PATH, ou en définissant la variables FOO_ROOT
   (var. cmake ou d'env. au choix)
   cf. [[https://cmake.org/cmake/help/latest/command/find_package.html#search-procedure]]

   Exemple d'un projet qui dépendrait de Chameleon
   [[https://gitlab.inria.fr/solverstack/distrib/-/tree/master/cmake/test/chameleon]]
