#!/bin/sh
set -ex

wget https://files.inria.fr/starpu/starpu-1.4.7/starpu-1.4.7.tar.gz
tar xvf starpu-1.4.7.tar.gz
cd starpu-1.4.7
./configure
make -j2 install
cd ..

wget https://gitlab.com/qr_mumps/qr_mumps/-/archive/3.1/qr_mumps-3.1.tar.gz
tar xvf qr_mumps-3.1.tar.gz
cd qr_mumps-3.1
cmake -B build -DQRM_WITH_STARPU=ON -DBUILD_SHARED_LIBS=ON
cmake --build build -j2
cmake --install build
cd ..
