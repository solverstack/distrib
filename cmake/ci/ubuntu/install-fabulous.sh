#!/bin/sh
set -ex
wget https://gitlab.inria.fr/api/v4/projects/2083/packages/generic/source/v1.1.4/fabulous-1.1.4.tar.gz
tar xvf fabulous-1.1.4.tar.gz
cd fabulous-1.1.4
cmake -B build -DFABULOUS_BUILD_C_API=ON -DFABULOUS_BUILD_Fortran_API=ON \
               -DFABULOUS_BUILD_EXAMPLES=ON -DFABULOUS_BUILD_TESTS=OFF -DBUILD_SHARED_LIBS=ON
cmake --build build -j2
cmake --install build
cd ..
