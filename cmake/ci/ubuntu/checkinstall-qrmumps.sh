#!/bin/sh
set -ex
export LD_LIBRARY_PATH=/usr/local/lib
cd cmake/test/qrmumps
cmake -B build
cmake --build build -j2
# segfault ./build/sqrm_least_squares_full
