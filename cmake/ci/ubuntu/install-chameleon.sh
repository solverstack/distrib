#!/bin/sh
set -ex

wget https://files.inria.fr/starpu/starpu-1.4.7/starpu-1.4.7.tar.gz
tar xvf starpu-1.4.7.tar.gz
cd starpu-1.4.7
./configure
make -j2 install
cd ..

wget https://gitlab.inria.fr/api/v4/projects/616/packages/generic/source/v1.3.0/chameleon-1.3.0.tar.gz
tar xvf chameleon-1.3.0.tar.gz
cd chameleon-1.3.0
cmake -B build -DCHAMELEON_USE_MPI=ON -DBUILD_SHARED_LIBS=ON
cmake --build build -j2
cmake --install build
cd ..
