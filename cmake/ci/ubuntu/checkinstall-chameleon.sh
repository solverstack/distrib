#!/bin/sh
set -ex
export LD_LIBRARY_PATH=/usr/local/lib
cd cmake/test/chameleon
cmake -B build
cmake --build build -j2
./build/test_chameleon
