#!/bin/sh
set -ex

# install parsec
./cmake/ci/ubuntu/install-parsec.sh

# install starpu
wget https://files.inria.fr/starpu/starpu-1.4.7/starpu-1.4.7.tar.gz
tar xvf starpu-1.4.7.tar.gz
cd starpu-1.4.7
./configure
make -j2 install
cd ..

# install pastix
wget https://files.inria.fr/pastix/releases/v6/pastix-6.4.0.tar.gz
tar xvf pastix-6.4.0.tar.gz
cd pastix-6.4.0
cmake -B build -DBUILD_SHARED_LIBS=ON -DBUILD_64bits=ON -DPASTIX_INT64=OFF -DPASTIX_WITH_MPI=ON -DPASTIX_WITH_STARPU=ON -DPASTIX_WITH_PARSEC=ON
cmake --build build -j2
cmake --install build
cd ..
