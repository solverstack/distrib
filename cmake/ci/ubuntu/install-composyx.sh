#!/bin/sh
set -ex

# install blaspp, lapackpp and arpack-ng
git clone -b 2022.05.00 https://bitbucket.org/icl/blaspp.git
git clone -b 2022.05.00 https://bitbucket.org/icl/lapackpp.git
git clone -b 3.8.0 https://github.com/opencollab/arpack-ng.git

cd blaspp
cmake -B build -DCMAKE_INSTALL_PREFIX=/usr/local
cmake --build build -j2
cmake --install build
cd ..

cd lapackpp
cmake -B build -DCMAKE_INSTALL_PREFIX=/usr/local
cmake --build build -j2
cmake --install build
cd ..

cd arpack-ng
cmake -B build -DICB=ON
cmake --build build -j2
cmake --install build
cd ..

# install starpu
wget https://files.inria.fr/starpu/starpu-1.4.7/starpu-1.4.7.tar.gz
tar xvf starpu-1.4.7.tar.gz
cd starpu-1.4.7
./configure
make -j2 install
cd ..

# install pastix
wget https://files.inria.fr/pastix/releases/v6/pastix-6.4.0.tar.gz
tar xvf pastix-6.4.0.tar.gz
cd pastix-6.4.0
cmake -B build -DBUILD_SHARED_LIBS=ON -DBUILD_64bits=ON -DPASTIX_INT64=OFF \
               -DPASTIX_WITH_MPI=ON -DPASTIX_WITH_STARPU=ON -DPASTIX_WITH_PARSEC=OFF
cmake --build build -j2
cmake --install build
cd ..

# install composyx
wget https://gitlab.inria.fr/api/v4/projects/52455/packages/generic/source/v1.0.1/composyx-1.0.1.tar.gz
tar xvf composyx-1.0.1.tar.gz
cd composyx-1.0.1
cmake -B build -DCOMPOSYX_COMPILE_EXAMPLES=OFF -DCOMPOSYX_COMPILE_TESTS=OFF -DCOMPOSYX_USE_ARPACK=OFF \
               -DBUILD_SHARED_LIBS=ON
cmake --build build -j2
cmake --install build
cd ..
