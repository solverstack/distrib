#!/bin/sh
set -ex
export LD_LIBRARY_PATH=/usr/local/lib
cd cmake/test/composyx
cmake -B build
cmake --build build -j2
./build/test_composyx
./build/test_driver_c
./build/test_driver_f
