#!/bin/sh
set -ex
export LD_LIBRARY_PATH=/usr/local/lib
cd cmake/test/pastix
cmake -B build
cmake --build build -j2
./build/test_pastix --lap 100
