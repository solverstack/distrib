#!/bin/sh
set -ex
export LD_LIBRARY_PATH=/usr/local/lib
cd cmake/test/fabulous
cmake -B build
cmake --build build -j2
./build/test_fabulous_cpp
./build/test_fabulous_c
