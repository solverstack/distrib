#!/bin/sh
set -ex
# qr_mumps rely on metis, scotch and colamd (in suitesparse)
brew install metis scotch suitesparse

# install starpu (may be missing)
brew install --build-from-source ~/brew-repo/starpu.rb

# install qrmumps
wget https://gitlab.com/qr_mumps/qr_mumps/-/archive/3.1/qr_mumps-3.1.tar.gz
tar xvf qr_mumps-3.1.tar.gz
cd qr_mumps-3.1
cmake -B build -DBLAS_LIBRARIES="/usr/local/Cellar/openblas/0.3.26/lib/libopenblas.dylib;-lm;-ldl" \
               -DLAPACK_LIBRARIES="/usr/local/Cellar/openblas/0.3.26/lib/libopenblas.dylib;-lm;-ldl" \
               -DQRM_WITH_STARPU=ON -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=$PWD/install
cmake --build build -j2
cmake --install build
cd ..
