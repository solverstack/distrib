#!/bin/sh
set -ex
git clone https://bitbucket.org/mfaverge/parsec.git
cd parsec
git checkout mymaster
cmake -B build -DCMAKE_BUILD_TYPE=Debug -DBUILD_SHARED_LIBS=ON \
               -DPARSEC_GPU_WITH_CUDA=OFF -DPARSEC_DIST_WITH_MPI=ON
cmake --build build -j2
cmake --install build
cd ..
