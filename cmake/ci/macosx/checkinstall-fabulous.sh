#!/bin/sh
set -ex
export CMAKE_PREFIX_PATH=$PWD/fabulous-1.1.4/install
cd cmake/test/fabulous
cmake -B build -DBLA_PREFER_PKGCONFIG=ON
cmake --build build -j2
./build/test_fabulous_cpp
./build/test_fabulous_c
