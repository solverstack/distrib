#!/bin/sh
set -ex
export CMAKE_PREFIX_PATH=$PWD/chameleon-1.3.0/install
cd cmake/test/chameleon
cmake -B build -DBLA_PREFER_PKGCONFIG=ON
cmake --build build -j2
./build/test_chameleon
