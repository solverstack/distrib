#!/bin/sh
set -ex
# install blaspp lapackpp arpack-ng and composyx with cmake
# This can be done only occasionally directly on the vm to update versions of blaspp, lapackpp and arpack-ng
# BLAS_LIBRARIES="-DBLAS_LIBRARIES=-L/usr/local/opt/openblas/lib/ -lopenblas";
# LAPACK_LIBRARIES="-DLAPACK_LIBRARIES=-L/usr/local/opt/openblas/lib -lopenblas"
# git clone https://bitbucket.org/icl/blaspp.git
# git clone https://bitbucket.org/icl/lapackpp.git
# git clone https://github.com/opencollab/arpack-ng.git
#
# cd blaspp
# mkdir build && cd build
# cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local $BLAS_LIBRARIES
# make install
# cd ../..
#
# cd lapackpp
# mkdir build && cd build
# cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local $LAPACK_LIBRARIES
# make install
# cd ../..
#
# cd arpack-ng
# mkdir build && cd build
# cmake .. -DICB=ON $BLAS_LIBRARIES $LAPACK_LIBRARIES
# make install
# cd ../..

# install composyx dependencies packages in our brew repository
brew install -v --cc=clang --build-from-source ~/brew-repo/blaspp.rb
brew install -v --cc=clang --build-from-source ~/brew-repo/lapackpp.rb
brew install -v --cc=clang --build-from-source ~/brew-repo/arpackng.rb
brew install -v --cc=clang --build-from-source ~/brew-repo/fabulous.rb
brew install -v --cc=clang --build-from-source ~/brew-repo/pastix.rb

# official mumps distribution on macosx not found
# TODO: find one and enable mumps for composyx

# install composyx
wget https://gitlab.inria.fr/api/v4/projects/52455/packages/generic/source/v1.0.1/composyx-1.0.1.tar.gz
tar xvf composyx-1.0.1.tar.gz
cd composyx-1.0.1

cmake -B build -DCOMPOSYX_COMPILE_EXAMPLES=OFF -DCOMPOSYX_COMPILE_TESTS=OFF \
               -DCOMPOSYX_USE_MUMPS=OFF -DCOMPOSYX_USE_PASTIX=ON \
               -DBUILD_SHARED_LIBS=ON -DBLA_PREFER_PKGCONFIG=ON \
               -DCMAKE_PREFIX_PATH=/usr/local/opt/openblas -DCMAKE_INSTALL_PREFIX=$PWD/install
cmake --build build -j2
cmake --install build
cd ..
