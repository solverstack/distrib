#!/bin/sh
set -ex
export CMAKE_PREFIX_PATH=$PWD/pastix-6.4.0/install
cd cmake/test/pastix
cmake -B build -DBLA_PREFER_PKGCONFIG=ON
cmake --build build -j2
./build/test_pastix --lap 100
