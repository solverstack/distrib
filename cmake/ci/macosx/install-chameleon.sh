#!/bin/sh
set -ex
# install starpu (may be missing)
brew install -v --cc=clang --build-from-source ~/brew-repo/starpu.rb

# install chameleon
wget https://gitlab.inria.fr/api/v4/projects/616/packages/generic/source/v1.3.0/chameleon-1.3.0.tar.gz
tar xvf chameleon-1.3.0.tar.gz
cd chameleon-1.3.0
cmake -B build -DCHAMELEON_USE_MPI=ON -DBLA_PREFER_PKGCONFIG=ON \
               -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=$PWD/install
cmake --build build -j2
cmake --install build
cd ..
