#!/bin/sh
set -ex
# This build is broken because cmake can't find openmp in recent homebrew
# cf. https://gitlab.kitware.com/cmake/cmake/-/issues/24097

if brew ls --versions fabulous > /dev/null; then brew remove --force --ignore-dependencies fabulous; fi

wget https://gitlab.inria.fr/api/v4/projects/2083/packages/generic/source/v1.1.4/fabulous-1.1.4.tar.gz
tar xvf fabulous-1.1.4.tar.gz
cd fabulous-1.1.4
cmake -B build -DBLA_PREFER_PKGCONFIG=ON -DFABULOUS_BUILD_C_API=ON -DFABULOUS_BUILD_Fortran_API=ON \
               -DFABULOUS_BUILD_EXAMPLES=ON -DFABULOUS_BUILD_TESTS=OFF \
               -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=$PWD/install
cmake --build build -j2
cmake --install build
cd ..
