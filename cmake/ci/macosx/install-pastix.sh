#!/bin/sh
set -ex
# install starpu (may be missing)
brew install --build-from-source ~/brew-repo/starpu.rb

# install parsec
#./cmake/ci/macosx/install-parsec.sh

# install pastix
wget https://files.inria.fr/pastix/releases/v6/pastix-6.4.0.tar.gz
tar xvf pastix-6.4.0.tar.gz
cd pastix-6.4.0
cmake -B build -DBUILD_SHARED_LIBS=ON -DBUILD_DOCUMENTATION=OFF \
               -DBUILD_64bits=ON -DPASTIX_INT64=OFF \
               -DPASTIX_WITH_MPI=ON -DPASTIX_WITH_STARPU=ON -DPASTIX_WITH_PARSEC=OFF \
               -DBLA_PREFER_PKGCONFIG=ON -DCMAKE_INSTALL_PREFIX=$PWD/install
cmake --build build -j2
cmake --install build
cd ..
