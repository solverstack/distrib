#!/bin/sh
set -ex
export CMAKE_PREFIX_PATH=$PWD/qr_mumps-3.1/install
cd cmake/test/qrmumps
cmake -B build -DBLA_PREFER_PKGCONFIG=ON
cmake --build build -j2
# segfault ./build/sqrm_least_squares_full
