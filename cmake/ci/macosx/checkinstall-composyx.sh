#!/bin/sh
set -ex
export CMAKE_PREFIX_PATH=$PWD/composyx-1.0.1/install
cd cmake/test/composyx
cmake -B build -DBLA_PREFER_PKGCONFIG=ON
cmake --build build -j2
./build/test_composyx
./build/test_driver_c
./build/test_driver_f
