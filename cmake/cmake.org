L'ensemble des logiciels utilisent CMake pour la
compilation/installation.  Depuis 2013 un effort soutenus de
modernisation des projets CMake a été effectué afin de:
1) Améliorer la détection des dépendances comme Hwloc, StarPU, Parsec,
   Scotch, les Blas/Lapack + interfaces C (cblas, lapacke), etc. Ceci
   a été rendu possible par le développement du projet [[https://gitlab.inria.fr/solverstack/morse_cmake][morse_cmake]] qui
   est utilisable comme un git submodule dans les projets et permet
   d'avoir des fichiers de module CMake supplémentaires pour améliorer
   les détections.
2) Moderniser les projets CMake afin d'utiliser les "target" CMake et
   d'éviter le plus possible les mauvaises pratiques telle que
   l'emploi de listes globales pour la gestion des options de
   compilations, définitions, link. Voir [[cmake/MAJ_CMAKE.org]].
3) Garantir que chaque projet CMake fournisse à l'installation un
   fichier de configuration FooConfig.cmake permettant aux
   utilisateurs CMake de se lier facilement à nos solveurs.

L'installation de nos solveurs via CMake peuvent être testés par un
job gitlab-ci présent dans ce git, voir les [[https://gitlab.inria.fr/solverstack/distrib/-/pipelines][pipelines]] et le fichier
gitlab-ci associé [[../.gitlab-ci.yml]]. Les mini-projets CMake permettant
d'émuler la détection de nos logiciels par un utilisateur sont dans
[[cmake/test][cmake/test]].
