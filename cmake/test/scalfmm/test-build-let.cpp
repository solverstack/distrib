﻿// @FUSE_MPI
#include <array>
#include <chrono>
#include <thread>

#include "scalfmm/container/particle.hpp"
#include "scalfmm/container/particle_container.hpp"
#include "scalfmm/tools/fma_dist_loader.hpp"
#include "scalfmm/tools/fma_loader.hpp"
#include "scalfmm/tree/box.hpp"

#include "scalfmm/interpolation/uniform.hpp"
#include "scalfmm/matrix_kernels/laplace.hpp"

#include "scalfmm/tree/cell.hpp"
#include "scalfmm/tree/dist_group_tree.hpp"
#include "scalfmm/tree/group_let.hpp"
#include "scalfmm/tree/leaf.hpp"

#include <cpp_tools/parallel_manager/parallel_manager.hpp>
#include <cpp_tools/cl_parser/tcli.hpp>
#include <cpp_tools/cl_parser/help_descriptor.hpp>
#include <cpp_tools/colors/colorized.hpp>
///
/// \brief main
/// \param argv
/// \return
///
/// \code
/// ./examples/RelWithDebInfo/test-build-let --input-file ../data/prolate.fma
/// --order 3 --tree-height 4 --group-size 3
///   mpirun --oversubscribe -np 3 ./examples/RelWithDebInfo/test-build-let
///   --input-file ../buildMPI/prolate.fma --order 3 --tree-height 3
///   --group-size 3 --d 3
/// \endcode
namespace local_args
{
    struct tree_height : cpp_tools::cl_parser::required_tag
    {
        cpp_tools::cl_parser::str_vec flags = {"--tree-height", "-th"};
        const char* description = "Tree height (or initial height in case of an adaptive tree).";
        using type = int;
        type def = 2;
    };

    struct order : cpp_tools::cl_parser::required_tag
    {
        cpp_tools::cl_parser::str_vec flags = {"--order", "-o"};
        const char* description = "Precision setting.";
        using type = std::size_t;
        type def = 3;
    };

    struct thread_count
    {
        cpp_tools::cl_parser::str_vec flags = {"--threads", "-t"};
        const char* description = "Maximum thread count to be used.";
        using type = int;
        type def = 1;
    };

    struct input_file : cpp_tools::cl_parser::required_tag
    {
        cpp_tools::cl_parser::str_vec flags = {"--input-file", "-fin"};
        const char* description = "Input filename (.fma or .bfma).";
        using type = std::string;
    };

    struct output_file
    {
        cpp_tools::cl_parser::str_vec flags = {"--output-file", "-fout"};
        const char* description = "Output particle file (with extension .fma (ascii) or bfma (binary).";
        using type = std::string;
    };

    struct log_level
    {
        cpp_tools::cl_parser::str_vec flags = {"--log-level", "-llog"};
        const char* description = "Log level to print.";
        using type = std::string;
        type def = "info";
    };

    struct log_file
    {
        cpp_tools::cl_parser::str_vec flags = {"--log-file", "-flog"};
        const char* description = "Log to file using spdlog.";
        using type = std::string;
        type def = "";
    };

    struct block_size
    {
        cpp_tools::cl_parser::str_vec flags = {"--group-size", "-gs"};
        const char* description = "Group tree chunk size.";
        using type = int;
        type def = 250;
    };

    struct Dimension
    {
        cpp_tools::cl_parser::str_vec flags = {"--dimension", "--d"};
        const char* description = "Dimension : \n   2 for dimension 2, 3 for dimension 3";
        using type = int;
        type def = 1;
    };
    struct PartDistrib
    {
        /// Unused type, mandatory per interface specification
        using type = bool;
        /// The parameter is a flag, it doesn't expect a following value
        enum
        {
            flagged
        };
        cpp_tools::cl_parser::str_vec flags = {"--dist_part"};
        std::string description = "Use the particle distribution to distribute the tree";
    };
    struct PartLeafDistrib
    {
        /// Unused type, mandatory per interface specification
        using type = bool;
        /// The parameter is a flag, it doesn't expect a following value
        enum
        {
            flagged
        };
        cpp_tools::cl_parser::str_vec flags = {"--dist_part_leaf"};
        std::string description = "Use two distribution one for the particle and one for the tree";
    };
}   // namespace local_args
template<int dimension>
auto run(cpp_tools::parallel_manager::parallel_manager& para, const std::string& input_file, const std::string& output_file, const int tree_height,
         const int& part_group_size, const int& leaf_group_size, const int order, bool use_leaf_distribution,
         bool use_particle_distribution) -> int
{
    constexpr int nb_inputs_near = 1;
    constexpr int nb_outputs_near = 1;
    constexpr int nb_inputs_far = 1;
    constexpr int nb_outputs_far = 1;
    using value_type = double;
    using mortonIndex_type = std::size_t;
    using globalIndex_type = std::size_t;

    using matrix_kernel_type = scalfmm::matrix_kernels::laplace::one_over_r;

    using interpolator_type = scalfmm::interpolation::uniform_interpolator<double, dimension, matrix_kernel_type>;

    using particle_type = scalfmm::container::particle<value_type, dimension, value_type, nb_inputs_near, value_type,
                                                       nb_outputs_near /*, mortonIndex_type, globalIndex_type*/>;
    using read_particle_type = scalfmm::container::particle<value_type, dimension, value_type, nb_inputs_near,
                                                            value_type, 0, mortonIndex_type, globalIndex_type>;
    using container_type = scalfmm::container::particle_container<particle_type>;
    using position_type = typename particle_type::position_type;
    //    using cell_type =
    //      scalfmm::component::cell<value_type, dimension, nb_inputs_far, nb_outputs_far, std::complex<value_type>>;
    using cell_type = scalfmm::component::cell<typename interpolator_type::storage_type>;
    using leaf_type = scalfmm::component::leaf<particle_type>;
    using box_type = scalfmm::component::box<position_type>;
    using group_tree_type = scalfmm::component::dist_group_tree<cell_type, leaf_type, box_type>;

    ///
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ///   Read the data in parallel
    ///
    ///   1) read constants of the problem in file;
    ///   2) each processor read N/P particles
    scalfmm::tools::DistFmaGenericLoader<value_type, dimension> loader(input_file, para, para.io_master());
    //
    const std::size_t number_of_particles = loader.getNumberOfParticles();
    const int local_number_of_particles = loader.getMyNumberOfParticles();
    value_type width = loader.getBoxWidth();
    auto centre = loader.getBoxCenter();
    auto nb_val_to_red_per_part = loader.get_dimension() + loader.get_number_of_input_per_record();
    double* values_to_read = new double[nb_val_to_red_per_part]{};
    box_type box(width, centre);
    //
    container_type container(local_number_of_particles);
    std::vector<particle_type> particles_set(local_number_of_particles);
    for(std::size_t idx = 0; idx < local_number_of_particles; ++idx)
    {
        loader.fillParticle(values_to_read, nb_val_to_red_per_part);
        particle_type p;
        std::size_t ii{0};
        for(auto& e: p.position())
        {
            e = values_to_read[ii++];
        }
        // p.variables()
        particles_set[idx] = p;
    }
    ///
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    // check
    //    for(std::size_t idx = 0; idx < 10; ++idx)
    //    {
    //        std::cout << idx << " p " << container.particle(idx) << std::endl;
    //    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ///    Set particles in the tree and construct the let
    ///  1) sort the particles according to their Morton index
    ///  2) construct the tree athen the let
    ///
    group_tree_type* letGroupTree = nullptr;
    //

    int rank = para.get_process_id();

    int leaf_level = tree_height - 1;

    scalfmm::tree::let::buildLetTree(para, number_of_particles, particles_set, box, leaf_level, part_group_size,
                                     leaf_group_size, letGroupTree, order, use_leaf_distribution,
                                     use_particle_distribution);

#ifdef SCALFMM_USE_MPI
    std::cout << std::flush;
    para.get_communicator().barrier();
#endif

    if(para.io_master())
    {
        letGroupTree->print_distrib(std::cout);
    }

    ///
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ///
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ///   Save the data
    const int nbDataPerRecord = scalfmm::container::particle_traits<particle_type>::number_of_elements;
    const int inputs_size = scalfmm::container::particle_traits<particle_type>::inputs_size;

    // static constexpr std::size_t nbDataPerRecord = particle_type::number_of_elements;
    scalfmm::tools::DistFmaGenericWriter<value_type> writer(output_file, para);
    /// Get the number of particles
    std::cout << "number_of_particles " << number_of_particles << std::endl;
    ///
    writer.writeHeader(centre, width, number_of_particles, sizeof(value_type), nbDataPerRecord, dimension, inputs_size);
    ///
    writer.writeFromTree(letGroupTree, number_of_particles);
    ///
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    delete letGroupTree;

    return 0;
}

auto main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) -> int
{
    cpp_tools::parallel_manager::parallel_manager para;
    para.init();

    //
    // Parameter handling
    auto parser =
      cpp_tools::cl_parser::make_parser(cpp_tools::cl_parser::help{}, local_args::input_file(), local_args::output_file(), local_args::tree_height{},
                               local_args::order{},   // args::thread_count{},
                               local_args::block_size{}, local_args::log_file{}, local_args::log_level{}, local_args::Dimension{},
                               local_args::PartDistrib{}, local_args::PartLeafDistrib{});
    parser.parse(argc, argv);
    // Getting command line parameters
    const int tree_height{parser.get<local_args::tree_height>()};

    const int group_size{parser.get<local_args::block_size>()};
    const std::string input_file{parser.get<local_args::input_file>()};

    const auto output_file{parser.get<local_args::output_file>()};
    const auto order{parser.get<local_args::order>()};
    const auto dimension{parser.get<local_args::Dimension>()};

    bool use_particle_distribution{parser.exists<local_args::PartDistrib>()};
    bool use_leaf_distribution{!use_particle_distribution};
    if(parser.exists<local_args::PartLeafDistrib>())
    {
        use_leaf_distribution = true;
        use_particle_distribution = true;
    }

    if(para.io_master())
    {
        std::cout << cpp_tools::colors::blue << "<params> Tree height: " << tree_height << cpp_tools::colors::reset << '\n';
        std::cout << cpp_tools::colors::blue << "<params> Group Size:  " << group_size << cpp_tools::colors::reset << '\n';
        std::cout << cpp_tools::colors::blue << "<params> order:       " << order << cpp_tools::colors::reset << '\n';
        if(!input_file.empty())
        {
            std::cout << cpp_tools::colors::blue << "<params> Input file:  " << input_file << cpp_tools::colors::reset
                      << '\n';
        }
        std::cout << cpp_tools::colors::blue << "<params> Output file: " << output_file << cpp_tools::colors::reset << '\n';
        std::cout << cpp_tools::colors::blue << "<params> Particle Distribution: " << std::boolalpha
                  << use_particle_distribution << cpp_tools::colors::reset << '\n';
        std::cout << cpp_tools::colors::blue << "<params> Leaf Distribution:     " << std::boolalpha
                  << use_leaf_distribution << cpp_tools::colors::reset << '\n';
    }
    switch(dimension)
    {
    case 2:
    {
        constexpr int dim = 2;

        run<dim>(para, input_file, output_file, tree_height, group_size, group_size, order, use_leaf_distribution,
                 use_particle_distribution);
        break;
    }
    case 3:
    {
        constexpr int dim = 3;

        run<dim>(para, input_file, output_file, tree_height, group_size, group_size, order, use_leaf_distribution,
                 use_particle_distribution);
        break;
    }
    default:
    {
        std::cerr << "Dimension should b 2 or 4 !!\n";
    }
    }
    para.end();
}
