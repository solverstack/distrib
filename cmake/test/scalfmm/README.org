#+TITLE: HOWTO

Install ScalFMM branch 'experimental'
#+begin_src sh
# install first GNU CXX compiler, blas, fftw, mpi, ex :
# apt install build-essential libopenblas-dev libfftw3-dev libopenmpi-dev
git clone --recursive -b experimental https://gitlab.inria.fr/solverstack/ScalFMM.git
cd ScalFMM/Build
export SCALFMM_ROOT=$PWD/install
cmake ../experimental/ -Dscalfmm_USE_MPI=ON -DCMAKE_INSTALL_PREFIX=$SCALFMM_ROOT
make -j5 install
cd ../..
#+end_src

#+begin_src sh
git clone https://gitlab.inria.fr/solverstack/distrib.git
cd cmake/test/scalfmm
mkdir build
cd build
cmake .. # -DCMAKE_PREFIX_PATH=$SCALFMM_ROOT/lib/cmake/scalfmm
make
./test_scalfmm
#+end_src
