#ifndef FABULOUS_MATRIX_MARKET_LOADER_HPP
#define FABULOUS_MATRIX_MARKET_LOADER_HPP

#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <iostream>
#include <fstream>
#include <sstream>
#include <type_traits>
#include <complex>
#include <vector>

#include "fabulous/utils/Error.hpp"
#include "fabulous/utils/Utils.hpp"
#include "fabulous/utils/Meta.hpp"

#include "BadMatrix.hpp"

namespace fabulous {

/*! \brief Loader for matrix in matrix-market format */
class MatrixMarketLoader
{
private:
    template<class Matrix,
             class U = typename Matrix::value_type,
             class T = typename Matrix::primary_type,
             class = typename std::enable_if< // complex types only
                 std::is_same<U, std::complex<T>>::value>::type >
    void load_line(Matrix &M, const std::vector<std::string> &v, bool symmetric)
    {
        int i = std::stoi(v[0]) - 1;
        int j   = std::stoi(v[1]) - 1;

        T real     = T(std::stod(v[2]));
        T imag     = T(std::stod(v[3]));

        M.at(i, j) =  U{real, imag};
        if (symmetric)
            M.at(j, i) = U{real, imag};
    }

    template<class Matrix,
             class T = typename Matrix::value_type,
             class = typename std::enable_if< // real types only:
                 std::is_same<T, float>::value ||
                 std::is_same<T, double>::value>::type >
    void load_line(Matrix &M, const std::vector<std::string> &v, bool symmetric)
    {
        int col = std::stoi(v[0]) - 1;
        int row = std::stoi(v[1]) - 1;
        T val   = T(std::stod(v[2]));

        M.at(row, col) = val;
        if (symmetric)
            M.at(col, row) = val;
    }

    bool header_match(const std::vector<std::string> &header, std::string token)
    {
        return ( std::find(header.begin(), header.end(), token) != header.end() );
    }

    template<class Matrix>
    void LoadMatrix(Matrix &A, std::ifstream &file, const std::string &filename)
    {
        if ( !file.is_open() ) {
            FABULOUS_THROW(File, filename << ": file not opened");
        }

        bool commentLine;
        bool symmetric = false;
        std::string line;

        getline(file, line);
        auto header = split(line);
        if (header[0] != "%%MatrixMarket") {
            FABULOUS_THROW(Input, filename <<": unrecognized file format.");
        }

        if (header_match(header, "symmetric"))
            symmetric = true;
        using S = typename Matrix::value_type;
        if (is_complex(Type<S>{}) && !header_match(header, "complex")) {
            FABULOUS_THROW(Input,
                           "This is a complex parser but file "
                           "header does not have \"complex\" tag");
        } else if (!is_complex(Type<S>{}) && !header_match(header, "real")) {
            FABULOUS_THROW(Input,
                           "This is a real parser but file "
                           "header does not have \"real\" tag");
        }

        do {
            getline(file,line);
            std::cerr << line << "\n";
            commentLine = ( line.length() == 0 || line[0] == '%' );
        } while (commentLine);
        std::cerr << std::flush;

        auto size = split(line);
        int N = std::stoi(size[0]);
        int M = std::stoi(size[1]);
        int NNZ = std::stoi(size[2]);

        // Check square ?
        if (N != M) {
            FABULOUS_THROW(Input,
                           "N != M :: "<< N <<" != "<<M
                           <<". Matrix is not square.");
        }

        A.resize(N, N, NNZ);
        for (int i = 0; i < NNZ; ++i) {
            getline(file, line);
            auto lv = split(line);
            load_line(A, lv, symmetric);
        }
    }

public:
    template<class Matrix>
    void LoadMatrix(Matrix &A, const std::string &filename)
    {
        if ( is_bad_matrix_t<Matrix>::value ) {
            FABULOUS_THROW(Parameter, "Use BadMatrix with BadLoader only");
        }
 
        std::ifstream file{filename, std::ios::in};
        if (!file) {
            std::stringstream ss;
            ss << filename <<": " << strerror(errno);
            fabulous::fatal_error(ss.str());
            return;
        }
        LoadMatrix(A, file, filename);
        file.close();
    }

}; // end class MatrixMarketLoader

} // end namespace fabulous

#endif // FABULOUS_MATRIX_MARKET_LOADER_HPP
