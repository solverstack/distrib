cmake_minimum_required(VERSION 3.5)

# to be able to use Fabulous_ROOT env. var.
cmake_policy(SET CMP0074 NEW)

project(TEST_FABULOUS C CXX Fortran)

# look for FABULOUS on the system
# Hint: use FABULOUS_ROOT (env. var. or cmake var.) to the installation directory of
# FABULOUS if not installed in a standard path
find_package(Fabulous REQUIRED)

if (TARGET FABULOUS::fabulous_cpp)
  get_target_property(_INCLUDES FABULOUS::fabulous_cpp INTERFACE_INCLUDE_DIRECTORIES)
  get_target_property(_DIRECTORIES FABULOUS::fabulous_cpp INTERFACE_LINK_DIRECTORIES)
  get_target_property(_LIBRARIES FABULOUS::fabulous_cpp INTERFACE_LINK_LIBRARIES)
  get_target_property(_CFLAGS FABULOUS::fabulous_cpp INTERFACE_COMPILE_OPTIONS)
  get_target_property(_LDFLAGS FABULOUS::fabulous_cpp INTERFACE_LINK_OPTIONS)

  message(STATUS "Distrib: Fabulous_BIN_DIR ${Fabulous_BIN_DIR}")
  message(STATUS "Distrib: Fabulous_INC_DIR ${Fabulous_INC_DIR}")
  message(STATUS "Distrib: Fabulous_LIB_DIR ${Fabulous_LIB_DIR}")

  message(STATUS "Distrib: IMPORTED TARGET FABULOUS::fabulous_cpp INTERFACE_INCLUDE_DIRECTORIES ${_INCLUDES}")
  message(STATUS "Distrib: IMPORTED TARGET FABULOUS::fabulous_cpp INTERFACE_LINK_DIRECTORIES ${_DIRECTORIES}")
  message(STATUS "Distrib: IMPORTED TARGET FABULOUS::fabulous_cpp INTERFACE_LINK_LIBRARIES ${_LIBRARIES}")
  message(STATUS "Distrib: IMPORTED TARGET FABULOUS::fabulous_cpp INTERFACE_COMPILE_OPTIONS ${_CFLAGS}")
  message(STATUS "Distrib: IMPORTED TARGET FABULOUS::fabulous_cpp INTERFACE_LINK_OPTIONS ${_LDFLAGS}")
else()
  message(FATAL_ERROR "Distrib: target FABULOUS::fabulous_cpp is not found, check your FabulousConfig.cmake.")
endif()

add_executable(test_fabulous_cpp test_fabulous.cpp)
target_link_libraries(test_fabulous_cpp PRIVATE FABULOUS::fabulous_cpp)

if (TARGET FABULOUS::fabulous_c_api)
  add_executable(test_fabulous_c test_fabulous.c)
  target_link_libraries(test_fabulous_c PRIVATE FABULOUS::fabulous_c_api)
endif()