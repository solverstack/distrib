#include <composyx.hpp>
#include <composyx/testing/TestMatrix.hpp>

using namespace composyx;

int main(int argc, char *argv[]) {

  using Scalar = double;

  /* Matrix product GEMM: C=AB */

  // Fill the matrix with A and B random values
  int M=1000;
  DenseMatrix<Scalar> A = test_matrix::random_matrix<Scalar, DenseMatrix<Scalar>>(M*M, Scalar{0}, Scalar{1}, M, M).matrix;
  DenseMatrix<Scalar> B = test_matrix::random_matrix<Scalar, DenseMatrix<Scalar>>(M*M, Scalar{0}, Scalar{1}, M, M).matrix;
  DenseMatrix<Scalar> C(M, M);

  // Compute the product (GEMM)
  C = A * B;

  return 0;
}