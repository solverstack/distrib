!! ##############################################################################################
!!
!! Copyright 2012-2020 CNRS, INPT
!! Copyright 2013-2015 UPS
!!  
!! This file is part of qr_mumps.
!!  
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as 
!! published by the Free Software Foundation, either version 3 of 
!! the License, or (at your option) any later version.
!!  
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!  
!! You can find a copy of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################

program sqrm_example

  use sqrm_mod
  implicit none

  
  type(sqrm_spmat_type)   :: qrm_spmat
  integer     , target    :: irn(13) = (/1, 1, 1, 2, 3, 3, 4, 4, 5, 5, 6, 7, 7/)
  integer     , target    :: jcn(13) = (/1, 3, 5, 2, 3, 5, 1, 4, 4, 5, 2, 1, 3/)
  real(r32), target    :: val(13) = (/1.d0, 2.d0, 3.d0, 1.d0, 1.d0, 2.d0, 4.d0, &
                                         1.d0, 5.d0, 1.d0, 3.d0, 6.d0, 1.d0/)  
  real(r32)            :: b(7)    = (/22.d0, 5.d0, 13.d0, 8.d0, 25.d0, 5.d0, 9.d0/)
  real(r32)            :: xe(5)   = (/1.d0, 2.d0, 3.d0, 4.d0, 5.d0/)
  real(r32)            :: x(5)    = qrm_szero
  real(r32)            :: r(7)    = qrm_szero
  integer                 :: info
  real(r32)               :: anrm, bnrm, xnrm, rnrm, onrm, fnrm
  
  call qrm_init()
  
  ! initialize the matrix data structure. 
  call qrm_spmat_init(qrm_spmat)

  qrm_spmat%m   =  7
  qrm_spmat%n   =  5
  qrm_spmat%nz  =  13
  qrm_spmat%irn => irn 
  qrm_spmat%jcn => jcn
  qrm_spmat%val => val

  r = b
  
  call qrm_vecnrm(b, size(b,1), '2', bnrm)

  call qrm_least_squares(qrm_spmat, b, x)

  call qrm_residual_norm(qrm_spmat, r, x, rnrm)
  call qrm_vecnrm(x, qrm_spmat%n, '2', xnrm)
  call qrm_spmat_nrm(qrm_spmat, 'f', anrm)
  call qrm_residual_orth(qrm_spmat, r, onrm)   

  write(*,'("Expected result is x= 1.00000 2.00000 3.00000 4.00000 5.00000")')
  write(*,'("Computed result is x=",5(1x,f7.5))')x

  xe = xe-x; 
  call qrm_vecnrm(xe, qrm_spmat%n, '2', fnrm)
  write(*,'(" ")')
  write(*,'("Forward error norm       ||xe-x||  = ",e7.2)')fnrm
  write(*,'("Optimality residual norm ||A^T*r|| = ",e7.2)')onrm

  call qrm_spmat_destroy(qrm_spmat)

  stop
end program sqrm_example
