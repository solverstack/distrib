!! ##############################################################################################
!!
!! Copyright 2012-2020 CNRS, INPT
!! Copyright 2013-2015 UPS
!!  
!! This file is part of qr_mumps.
!!  
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as 
!! published by the Free Software Foundation, either version 3 of 
!! the License, or (at your option) any later version.
!!  
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!  
!! You can find a copy of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################

program sqrm_example

  use sqrm_mod
  implicit none

  
  type(sqrm_spmat_type)   :: qrm_spmat
  type(sqrm_spfct_type)   :: qrm_spfct
  integer     , target    :: irn(10) = (/1, 1, 1, 1, 2, 3, 3, 4, 4, 5/)
  integer     , target    :: jcn(10) = (/1, 3, 4, 5, 2, 3, 5, 4, 5, 5/)
  real(r32), target    :: val(10) = (/53.d0, 8.d0, 4.d0, 3.d0, 10.d0, &
                                         6.d0, 8.d0, 26.d0, 5.d0, 14.d0/)  
  real(r32)            :: b(5)    = (/108.d0, 20.d0, 66.d0, 133.d0, 117.d0/)
  real(r32)            :: xe(5)   = (/1.d0, 2.d0, 3.d0, 4.d0, 5.d0/)
  real(r32)            :: x(5)    = qrm_szero
  real(r32)            :: r(5)    = qrm_szero
  integer                 :: info
  real(r32)               :: anrm, bnrm, xnrm, rnrm, fnrm
  
  call qrm_init()
  
  ! initialize the matrix data structure. 
  call qrm_spmat_init(qrm_spmat)

  qrm_spmat%m   =  5
  qrm_spmat%n   =  5
  qrm_spmat%nz  =  10
  qrm_spmat%irn => irn 
  qrm_spmat%jcn => jcn
  qrm_spmat%val => val
  qrm_spmat%sym =  1

  r = b
  x = b
  
  call qrm_vecnrm(b, size(b,1), '2', bnrm)

  call qrm_spfct_init(qrm_spfct, qrm_spmat)
  call qrm_analyse(qrm_spmat, qrm_spfct, qrm_no_transp)
  call qrm_factorize(qrm_spmat, qrm_spfct, qrm_no_transp)
  call qrm_solve(qrm_spfct, qrm_transp, x, b)
  call qrm_solve(qrm_spfct, qrm_no_transp, b, x)
  

  call qrm_residual_norm(qrm_spmat, r, x, rnrm)
  call qrm_vecnrm(x, qrm_spmat%n, '2', xnrm)
  call qrm_spmat_nrm(qrm_spmat, 'f', anrm)

  write(*,'("Expected result is x= 1.00000 2.00000 3.00000 4.00000 5.00000")')
  write(*,'("Computed result is x=",5(1x,f7.5))')x

  xe = xe-x; 
  call qrm_vecnrm(xe, qrm_spmat%n, '2', fnrm)
  write(*,'(" ")')
  write(*,'("Forward error norm       ||xe-x||  = ",e7.2)')fnrm
  write(*,'("Residual norm            ||A*x-b|| = ",e7.2)')rnrm

  call qrm_spmat_destroy(qrm_spmat)

  stop
end program sqrm_example
