#include <complex>
#include <chameleon.h>
int main() {
    CHAMELEON_Init(2, 0);
    int NB=10;
    CHAMELEON_Set( CHAMELEON_TILE_SIZE, NB );
    CHAM_desc_t* chamA;
    CHAMELEON_Desc_Create( &chamA, CHAMELEON_MAT_ALLOC_TILE, ChamComplexFloat, NB, NB, NB * NB, NB, NB, 0, 0, NB, NB, 1, 1 );
    CHAMELEON_cplrnt_Tile( chamA, NB );
    CHAMELEON_Finalize();
}