cmake_minimum_required(VERSION 3.5)

# to be able to use CHAMELEON_ROOT env. var.
cmake_policy(SET CMP0074 NEW)

project(TEST_CHAMELEON C CXX Fortran)

# look for CHAMELEON on the system
# Hint: use CHAMELEON_ROOT (env. var. or cmake var.) to the installation directory of
# CHAMELEON if not installed in a standard path
find_package(CHAMELEON REQUIRED)

if (TARGET CHAMELEON::chameleon)
  get_target_property(_INCLUDES CHAMELEON::chameleon INTERFACE_INCLUDE_DIRECTORIES)
  get_target_property(_DIRECTORIES CHAMELEON::chameleon INTERFACE_LINK_DIRECTORIES)
  get_target_property(_LIBRARIES CHAMELEON::chameleon INTERFACE_LINK_LIBRARIES)
  get_target_property(_CFLAGS CHAMELEON::chameleon INTERFACE_COMPILE_OPTIONS)
  get_target_property(_LDFLAGS CHAMELEON::chameleon INTERFACE_LINK_OPTIONS)

  message(STATUS "Distrib: CHAMELEON_BIN_DIR ${CHAMELEON_BIN_DIR}")
  message(STATUS "Distrib: CHAMELEON_INC_DIR ${CHAMELEON_INC_DIR}")
  message(STATUS "Distrib: CHAMELEON_LIB_DIR ${CHAMELEON_LIB_DIR}")

  message(STATUS "Distrib: IMPORTED TARGET CHAMELEON::chameleon INTERFACE_INCLUDE_DIRECTORIES ${_INCLUDES}")
  message(STATUS "Distrib: IMPORTED TARGET CHAMELEON::chameleon INTERFACE_LINK_DIRECTORIES ${_DIRECTORIES}")
  message(STATUS "Distrib: IMPORTED TARGET CHAMELEON::chameleon INTERFACE_LINK_LIBRARIES ${_LIBRARIES}")
  message(STATUS "Distrib: IMPORTED TARGET CHAMELEON::chameleon INTERFACE_COMPILE_OPTIONS ${_CFLAGS}")
  message(STATUS "Distrib: IMPORTED TARGET CHAMELEON::chameleon INTERFACE_LINK_OPTIONS ${_LDFLAGS}")
else()
  message(FATAL_ERROR "Distrib: target CHAMELEON::chameleon is not found, check your CHAMELEONConfig.cmake.")
endif()

add_executable(test_chameleon test_chameleon.c)
target_link_libraries(test_chameleon PRIVATE CHAMELEON::chameleon MORSE::LAPACKE)

# Hint: chameleon handles parallelism, deactivate multithreading in blas/lapack
# export OPENBLAS_NUM_THREADS=1 or export MKL_NUM_THREADS=1 depending on your blas/lapack vendor
# ./test_chameleon --n=6400 --nrhs=3200 --threads=2

find_package(MPI)

add_executable(test_chameleon_cpp test_chameleon.cpp)
target_link_libraries(test_chameleon_cpp PRIVATE CHAMELEON::chameleon MPI::MPI_CXX)
target_compile_definitions(test_chameleon_cpp PRIVATE CHAMELEON_COMPLEX_CPP)
