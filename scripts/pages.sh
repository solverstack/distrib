#!/bin/bash
mkdir -p public
cd website
touch README.org
emacs --batch --no-init-file --load publish.el --funcall org-publish-all
mv README.html ../public/index.html
cp -r figures/ ../public/
